package org.meisi.hadoop;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * Created by cardoso on 03/02/15.
 */
public class WikiUtils {
    public static Properties loadProps(Class className) {
        Properties configProp = new Properties();
        InputStream in = className.getResourceAsStream("/".concat("country.properties"));
        try {
            configProp.load(in);
        } catch (IOException e) {
            System.out.println("Error retrieving properties file");
            e.printStackTrace();
        }
        return configProp;
    }

    public static Set propertiesToMap(Properties props) {
        Set hm = new HashSet();
        for (java.util.Map.Entry<Object, Object> property : props.entrySet()) {
            hm.add(property.getValue());
        }
        return hm;
    }

}
