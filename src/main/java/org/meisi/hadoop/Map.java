package org.meisi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.mapreduce.Job;

import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cardoso on 02/02/15.
 */
public class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

    static enum Counters { INPUT_WORDS }

    private final static IntWritable one = new IntWritable(1);
    private final Text word = new Text();

    private long numRecords = 0;
    private String inputFile;

    private static Set countrySet;
    private final Logger logger = Logger.getLogger(Map.class.getName());

    public void configure(JobConf job) {
        Job jobF = null;
        Properties props = WikiUtils.loadProps(Map.class);
        if(countrySet == null){
            countrySet = WikiUtils.propertiesToMap(props);
        }
        try {
            jobF = Job.getInstance(job);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
        String line = value.toString().toLowerCase();
        Iterator itr = countrySet.iterator();
        while(itr.hasNext()){
            String country = (String) itr.next();
            int startIndex = line.indexOf(country);
            if(startIndex >= 0){
                int beforeStartIndex = startIndex-1;
                int afterStartIndex = startIndex + country.length();
                if(beforeStartIndex >= 0 && Character.isLetter(line.charAt(beforeStartIndex))){
                    return;
                }else if(Character.isLetter(line.charAt(afterStartIndex))){
                    return;
                }
                word.set(country);
                output.collect(word, one);
                reporter.incrCounter(Counters.INPUT_WORDS, 1);
            }
        }
    }
}

