/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.examples;

import org.meisi.hadoop.Map;
import org.meisi.hadoop.WikiUtils;
import scala.Tuple2;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import java.util.*;
import java.util.regex.Pattern;

import static org.meisi.hadoop.WikiUtils.*;

public final class SWordCount {
    private static final Pattern NEWLINE = Pattern.compile("\n");
    private static Set countrySet;

    public static void main(String[] args) throws Exception {

        if (args.length < 1) {
            System.err.println("Usage: SWordCount <file>");
            System.exit(1);
        }

        Properties props = loadProps(SWordCount.class);
        if(countrySet == null){
            countrySet = propertiesToMap(props);
        }

        SparkConf sparkConf = new SparkConf().setAppName("SWordCount");
        JavaSparkContext ctx = new JavaSparkContext(sparkConf);
        JavaRDD<String> lines = ctx.textFile("hdfs://master:8020/"+args[0]);

        JavaRDD<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterable<String> call(String s) {
                return Arrays.asList(NEWLINE.split(s));
            }
        });

        JavaPairRDD<String, Integer> ones = words.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String line) {
                Iterator itr = countrySet.iterator();
                while(itr.hasNext()){
                    String country = (String) itr.next();
                    int startIndex = line.indexOf(country);
                    if(startIndex >= 0){
                        int beforeStartIndex = startIndex-1;
                        int afterStartIndex = startIndex + country.length();
                        if(beforeStartIndex >= 0 && Character.isLetter(line.charAt(beforeStartIndex))){
                            break;
                        }else if(Character.isLetter(line.charAt(afterStartIndex))){
                            break;
                        }

                    }
                    return new Tuple2<String, Integer>(country, 1);
                }
                return null;
            }
        });

        JavaPairRDD<String, Integer> counts = ones.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer i1, Integer i2) {
                return i1 + i2;
            }
        });

        List<Tuple2<String, Integer>> output = counts.collect();
        for (Tuple2<?,?> tuple : output) {
            System.out.println(tuple._1() + ": " + tuple._2());
        }
        ctx.stop();
    }
}